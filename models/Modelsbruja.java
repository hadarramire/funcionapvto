// Made with Blockbench 4.4.2
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class Modelsbruja extends EntityModel<Entity> {
	private final ModelRenderer armorhead;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;

	public Modelsbruja() {
		textureWidth = 64;
		textureHeight = 64;

		armorhead = new ModelRenderer(this);
		armorhead.setRotationPoint(0.0F, 21.0F, 0.0F);
		armorhead.setTextureOffset(0, 19).addBox(4.0F, 0.0F, -4.0F, 2.0F, 1.0F, 8.0F, 0.0F, false);
		armorhead.setTextureOffset(24, 0).addBox(-4.0F, 0.0F, 4.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);
		armorhead.setTextureOffset(16, 11).addBox(-6.0F, 0.0F, -4.0F, 2.0F, 1.0F, 8.0F, 0.0F, false);
		armorhead.setTextureOffset(24, 3).addBox(-4.0F, 0.0F, -6.0F, 8.0F, 1.0F, 2.0F, 0.0F, false);
		armorhead.setTextureOffset(0, 0).addBox(-4.0F, 1.0F, -4.0F, 8.0F, 3.0F, 8.0F, 0.0F, false);
		armorhead.setTextureOffset(0, 13).addBox(-5.0F, 0.0F, -5.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		armorhead.setTextureOffset(0, 11).addBox(4.0F, 0.0F, -5.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		armorhead.setTextureOffset(4, 5).addBox(4.0F, 0.0F, 4.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		armorhead.setTextureOffset(0, 5).addBox(-5.0F, 0.0F, 4.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, -17.0F, -5.0F);
		armorhead.addChild(cube_r1);
		setRotationAngle(cube_r1, -0.2618F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(0, 0).addBox(-1.0F, 24.0F, -5.0F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, -17.0F, -5.0F);
		armorhead.addChild(cube_r2);
		setRotationAngle(cube_r2, -0.2182F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(12, 20).addBox(-2.0F, 22.0F, -4.0F, 4.0F, 3.0F, 4.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.0F, -17.0F, -5.0F);
		armorhead.addChild(cube_r3);
		setRotationAngle(cube_r3, -0.1309F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(0, 11).addBox(-3.0F, 21.0F, -2.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		armorhead.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
	}
}