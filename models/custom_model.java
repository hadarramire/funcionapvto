// Made with Blockbench 4.2.5
// Exported for Minecraft version 1.15 - 1.16 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class custom_model extends EntityModel<Entity> {
	private final ModelRenderer bb_main;

	public custom_model() {
		texWidth = 32;
		texHeight = 32;

		bb_main = new ModelRenderer(this);
		bb_main.setPos(0.0F, 24.0F, 0.0F);
		bb_main.texOffs(0, 0).addBox(-5.0F, -1.0F, 0.0F, 9.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.texOffs(0, 8).addBox(4.0F, -1.0F, -1.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);
		bb_main.texOffs(5, 8).addBox(5.0F, -1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.texOffs(0, 4).addBox(5.0F, -1.0F, -1.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		bb_main.texOffs(0, 2).addBox(-6.0F, -1.0F, -2.0F, 1.0F, 1.0F, 5.0F, 0.0F, false);
		bb_main.texOffs(7, 2).addBox(-7.0F, -1.0F, -1.0F, 1.0F, 1.0F, 3.0F, 0.0F, false);
		bb_main.texOffs(0, 12).addBox(-8.0F, -1.0F, 0.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);
	}

	@Override
	public void setupAnim(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void renderToBuffer(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}
}