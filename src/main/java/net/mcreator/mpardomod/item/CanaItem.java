
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.Item;
import net.minecraft.item.FishingRodItem;

import net.mcreator.mpardomod.itemgroup.MPARDOMODItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class CanaItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:cana")
	public static final Item block = null;

	public CanaItem(MpardomodModElements instance) {
		super(instance, 32);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemToolCustom() {
		}.setRegistryName("cana"));
	}

	private static class ItemToolCustom extends FishingRodItem {
		protected ItemToolCustom() {
			super(new Item.Properties().group(MPARDOMODItemGroup.tab).maxDamage(100));
		}

		@Override
		public int getItemEnchantability() {
			return 2;
		}
	}
}
