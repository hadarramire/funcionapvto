
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.mpardomod.itemgroup.MPARDOMODItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class EspadadaniproItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:espadadanipro")
	public static final Item block = null;

	public EspadadaniproItem(MpardomodModElements instance) {
		super(instance, 59);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 30;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return -1f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(DanyproItem.block));
			}
		}, 3, -3f, new Item.Properties().group(MPARDOMODItemGroup.tab)) {
		}.setRegistryName("espadadanipro"));
	}
}
