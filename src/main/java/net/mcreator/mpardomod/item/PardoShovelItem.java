
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.mpardomod.itemgroup.MPARDOMODItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class PardoShovelItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:pardo_shovel")
	public static final Item block = null;

	public PardoShovelItem(MpardomodModElements instance) {
		super(instance, 15);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ShovelItem(new IItemTier() {
			public int getMaxUses() {
				return 2500;
			}

			public float getEfficiency() {
				return 11f;
			}

			public float getAttackDamage() {
				return 0f;
			}

			public int getHarvestLevel() {
				return 5;
			}

			public int getEnchantability() {
				return 15;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(LingpardoItem.block));
			}
		}, 1, -3f, new Item.Properties().group(MPARDOMODItemGroup.tab).isImmuneToFire()) {
		}.setRegistryName("pardo_shovel"));
	}
}
