
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.UseAction;
import net.minecraft.item.Rarity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

import net.mcreator.mpardomod.itemgroup.MPARDOMODItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class PardocoinoroItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:pardocoinoro")
	public static final Item block = null;

	public PardocoinoroItem(MpardomodModElements instance) {
		super(instance, 1);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(MPARDOMODItemGroup.tab).maxStackSize(64).rarity(Rarity.COMMON));
			setRegistryName("pardocoinoro");
		}

		@Override
		public UseAction getUseAction(ItemStack itemstack) {
			return UseAction.EAT;
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
