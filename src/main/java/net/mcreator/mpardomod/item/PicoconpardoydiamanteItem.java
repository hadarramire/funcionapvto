
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.mpardomod.itemgroup.AdddItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class PicoconpardoydiamanteItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:picoconpardoydiamante")
	public static final Item block = null;

	public PicoconpardoydiamanteItem(MpardomodModElements instance) {
		super(instance, 256);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2500;
			}

			public float getEfficiency() {
				return 10f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 3;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(DiamantetrituradoItem.block), new ItemStack(Items.DIAMOND));
			}
		}, 1, -3f, new Item.Properties().group(AdddItemGroup.tab)) {
		}.setRegistryName("picoconpardoydiamante"));
	}
}
