
package net.mcreator.mpardomod.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.mpardomod.itemgroup.AdddItemGroup;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class PicodepardooscuroItem extends MpardomodModElements.ModElement {
	@ObjectHolder("mpardomod:picodepardooscuro")
	public static final Item block = null;

	public PicodepardooscuroItem(MpardomodModElements instance) {
		super(instance, 255);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 5001;
			}

			public float getEfficiency() {
				return 12f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 4;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(LingotedepardooscuroItem.block));
			}
		}, 1, -3f, new Item.Properties().group(AdddItemGroup.tab).isImmuneToFire()) {
		}.setRegistryName("picodepardooscuro"));
	}
}
