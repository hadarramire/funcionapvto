
package net.mcreator.mpardomod.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.mpardomod.item.LogoaddItem;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class AdddItemGroup extends MpardomodModElements.ModElement {
	public AdddItemGroup(MpardomodModElements instance) {
		super(instance, 238);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabaddd") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(LogoaddItem.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static ItemGroup tab;
}
