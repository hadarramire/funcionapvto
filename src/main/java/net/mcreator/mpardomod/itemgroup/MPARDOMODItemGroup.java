
package net.mcreator.mpardomod.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.mpardomod.item.MPARDOItem;
import net.mcreator.mpardomod.MpardomodModElements;

@MpardomodModElements.ModElement.Tag
public class MPARDOMODItemGroup extends MpardomodModElements.ModElement {
	public MPARDOMODItemGroup(MpardomodModElements instance) {
		super(instance, 195);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabmpardomod") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(MPARDOItem.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static ItemGroup tab;
}
