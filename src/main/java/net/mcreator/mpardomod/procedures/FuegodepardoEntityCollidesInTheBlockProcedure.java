package net.mcreator.mpardomod.procedures;

import net.minecraft.util.DamageSource;
import net.minecraft.entity.Entity;

import net.mcreator.mpardomod.MpardomodMod;

import java.util.Map;

public class FuegodepardoEntityCollidesInTheBlockProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MpardomodMod.LOGGER.warn("Failed to load dependency entity for procedure FuegodepardoEntityCollidesInTheBlock!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		entity.attackEntityFrom(DamageSource.IN_FIRE, (float) 1);
	}
}
