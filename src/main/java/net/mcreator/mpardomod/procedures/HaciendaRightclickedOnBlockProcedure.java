package net.mcreator.mpardomod.procedures;

import net.minecraft.world.IWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.item.ItemStack;

import net.mcreator.mpardomod.block.MariaplantBlock;
import net.mcreator.mpardomod.MpardomodMod;

import java.util.Random;
import java.util.Map;

public class HaciendaRightclickedOnBlockProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				MpardomodMod.LOGGER.warn("Failed to load dependency world for procedure HaciendaRightclickedOnBlock!");
			return;
		}
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				MpardomodMod.LOGGER.warn("Failed to load dependency x for procedure HaciendaRightclickedOnBlock!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				MpardomodMod.LOGGER.warn("Failed to load dependency y for procedure HaciendaRightclickedOnBlock!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				MpardomodMod.LOGGER.warn("Failed to load dependency z for procedure HaciendaRightclickedOnBlock!");
			return;
		}
		if (dependencies.get("itemstack") == null) {
			if (!dependencies.containsKey("itemstack"))
				MpardomodMod.LOGGER.warn("Failed to load dependency itemstack for procedure HaciendaRightclickedOnBlock!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");
		world.setBlockState(new BlockPos(x, y + 1, z), MariaplantBlock.block.getDefaultState(), 3);
		{
			ItemStack _ist = itemstack;
			if (_ist.attemptDamageItem((int) 1, new Random(), null)) {
				_ist.shrink(1);
				_ist.setDamage(0);
			}
		}
	}
}
